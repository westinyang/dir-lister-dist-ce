# DirectoryLister dist custom edition

DirectoryLister 发行定制版，基于v3.7.9，最后一个支持PHP7.2的版本，原版地址：https://github.com/DirectoryLister/DirectoryLister


## 修改说明

- 优化默认配置
- 优化顶部搜索
- 优化底部区域，文件位置 `app/views/components/footer.twig`
- 隐藏 `@eaDir` 文件夹，文件位置 `app/config/app.php`
  
## 其他说明

- !!! 安装时务必给 `cache` 文件夹权限 `sudo chmod 777 -R ./app/cache/`
- 隐藏文件配置，在根目录创建.hidden文件，一行一个，支持正则，[参考文档](https://docs.directorylister.com/configuration/hiding-files)
